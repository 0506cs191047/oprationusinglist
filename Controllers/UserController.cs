﻿using CrudOperationUsingList.Models;
using CrudOperationUsingList.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace CrudOperationUsingList.Controllers
{
    public class UserController : Controller
    {
        // Create Constructor
        // Call service Layer using IUserService
        // don't Use new keyword
        // Start using Constructor DI(dependency Injection)

        readonly IUserServices _userServices;
        public UserController(IUserServices userServices)
        {
            _userServices = userServices;
        }
        public IActionResult Index()
        {
            List<User> usersList = _userServices.GetAllUsers();
            return View(usersList);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(User user)
        {
            _userServices.AddUser(user);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            User user = _userServices.UserById(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(User user)
        {
            _userServices.Update(user);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            User user = _userServices.UserById(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Delete(User user)
        {
           _userServices.Delete(user);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            User user = _userServices.UserById(id);
            return View(user);
        }
        [HttpPost]
        public ActionResult Details() 
        {
            return RedirectToAction("Index");
        }
        
    }
}
