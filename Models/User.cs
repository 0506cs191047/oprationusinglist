﻿namespace CrudOperationUsingList.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
    }
}
