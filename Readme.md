﻿# CrudOperationUsingList ::----
    * First you Go the WebCore(MVC)
    * Going to create option and going to 5.0 version of .NET
    * You will create some folder ::----
          Views---->Controller---->Services---->Repository
    * Now you Add the classes into the Project.
         Controller ----> Controller empty ----> UserController.cs
         Model ----> Class ----> User.cs
         Services ----> Interface & Class ----> IUserServices & UserSevices
         Repository ----> Interface & Class ----> IUserRepository & UserRepository
    * Going to the Controller Layer ::---- 
         // Create Constructor
        // Call service Layer using IUserService
        // don't Use new keyword
        // Start using Constructor DI(dependency Injection)
        // Add a View with([HttpGet & [HttpPost]])
    * Going to the Services Layer ::---- 
        // Call Repository Layer of IUserRepository
        // Don't Create a new Keyword
        // Call Constructor and use Constructor DI
    * Going to the Services Repository ::---- 
         Add the Logic For All Operation.
    * Now, Going to Views ::-----
         Going to Index.cshtml ::----->
              @Html.ActionLink("Edit", "Edit", new { id=item.Id }) 
              @Html.ActionLink("Details", "Details", new { id=item.Id }) (@*Here we Personally Correct the id = item.Id*@. It reads the Id value)
              @Html.ActionLink("Delete", "Delete", new { id=item.Id })
    * Now. Going to Startup ::-----
        Going to Configuration Option ::-----
             public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddScoped<IUserServices,UserServices>();
            services.AddSingleton<IUserRepository, UserRepository>();
        }
