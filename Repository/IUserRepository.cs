﻿using CrudOperationUsingList.Models;
using System.Collections.Generic;

namespace CrudOperationUsingList.Repository
{
    public interface IUserRepository
    {
        bool AddUser(User user);
        bool Delete(User user);
        List<User> GetAllUsers();
        User GetUserByName(string name);
        bool Update(User user);
        User UserById(int id);
    }
}
