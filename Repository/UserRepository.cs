﻿using CrudOperationUsingList.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace CrudOperationUsingList.Repository
{
    public class UserRepository : IUserRepository
    {
        List<User> _users;
        public UserRepository() 
        { 
            _users= new List<User>();
        }

        public bool AddUser(User user)
        {
            // Auto incremented the Id

            if (_users.Count == 0)
            {
                user.Id = 1;
            }
            else
            {
                user.Id = _users.Max(u => u.Id) + 1;
            }
            _users.Add(user);
            return true;
        }

        public List<User> GetAllUsers()
        {
           return _users;
        }
        public User GetUserByName(string name)
        {
            return _users.Find(u => u.Name == name);
        }
        public User UserById(int id)
        {
            return _users.Find(u => u.Id == id);
        }

        public bool Update(User user)
        {
            User users = _users.Find(u => u.Id == user.Id);
            users.Name = user.Name;
            users.City = user.City;
            users.Region = user.Region;
            return true;
        }

        public bool Delete(User user)
        {
            var users = UserById(user.Id);
            return _users.Remove(users);
        }
    }
}
