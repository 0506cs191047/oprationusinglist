﻿using CrudOperationUsingList.Models;
using System.Collections.Generic;

namespace CrudOperationUsingList.Services
{
    public interface IUserServices
    {
        bool AddUser(User user);
        bool Delete(User user);
        List<User> GetAllUsers();
        bool Update(User user);
        User UserById(int id);
    }
}
