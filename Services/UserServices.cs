﻿using CrudOperationUsingList.Models;
using CrudOperationUsingList.Repository;
using System.Collections.Generic;
using System.Runtime.Intrinsics.X86;

namespace CrudOperationUsingList.Services
{
    public class UserServices : IUserServices
    {
        // Call Repository Layer of IUserRepository
        // Don't Create a new Keyword
        // Call Constructor and use Constructor DI
        readonly IUserRepository _userRepository;
        public UserServices(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool AddUser(User user)
        {
            User Exists = _userRepository.GetUserByName(user.Name);
            if (Exists == null)
            {
                _userRepository.AddUser(user);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(User user)
        {
            _userRepository.Delete(user);
            return true;
        }

        public List<User> GetAllUsers()
        {

            List<User> users = _userRepository.GetAllUsers();
            return users;
        }
        public bool Update(User user)
        {
            _userRepository.Update(user);
            return true;
        }

        public User UserById(int id)
        {
            User users = _userRepository.UserById(id);
            return users;
        }
    }
}
